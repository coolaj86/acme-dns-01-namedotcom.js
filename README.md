# [acme-dns-01-namedotcom.js](https://git.rootprojects.org/root/acme-dns-01-namedotcom.js) | a [Root](https://rootprojects.org/) project

Name.com DNS + Let's Encrypt for Node.js - ACME dns-01 challenges w/ ACME.js and Greenlock.js

Handles ACME dns-01 challenges. Compatible with ACME.js and Greenlock.js. Passes acme-dns-01-test.

# Features

-   Compatible
    -   Let’s Encrypt v2.1 / ACME draft 18 (2019)
    -   Name.com v4 API
    -   ACME.js, Greenlock.js, and others
-   Quality
    -   node v6 compatible VanillaJS
    -   < 150 lines of code
    -   Zero Dependencies

# Install

```js
npm install --save acme-dns-01-namedotcom
```

## Name.com API Token

-   Login to your account at: <https://www.name.com/>
-   Generate a token at <https://www.name.com/account/settings/api>
-   **Important** Enable API Access, _again_ at <https://www.name.com/account/settings/security>

The following error is what you may get if you have Two-Factor Auth and don't _Enable API Access_ the second time:

```json
{
	"message": "Permission Denied",
	"details": "Authentication Error - Account Has Namesafe Enabled"
}
```

If you're using the Whitelist IPs feature, don't forget to add your test environment!

# Usage

First you create an instance with your credentials:

```js
var dns01 = require('acme-dns-01-namedotcom').create({
	baseUrl: 'http://api.name.com/v4/', // default
	username: 'johndoe',
	token: 'xxxx'
});
```

Then you can use it with any compatible ACME library, such as Greenlock.js or ACME.js.

## Greenlock.js

```js
var Greenlock = require('greenlock-express');
var greenlock = Greenlock.create({
	challenges: {
		'dns-01': dns01
		// ...
	}
});
```

See [Greenlock Express](https://git.rootprojects.org/root/greenlock-express.js)
and/or [Greenlock.js](https://git.rootprojects.org/root/greenlock.js) documentation for more details.

## ACME.js

```js
// TODO
```

See the [ACME.js](https://git.rootprojects.org/root/acme-v2.js) for more details.

## Build your own

There are only 5 methods:

-   `init(config)`
-   `zones(opts)`
-   `set(opts)`
-   `get(opts)`
-   `remove(opts)`

```js
dns01
	.set({
		identifier: { value: 'foo.example.co.uk' },
		wildcard: false,
		dnsZone: 'example.co.uk',
		dnsPrefix: '_acme-challenge.foo',
		dnsAuthorization: 'xxx_secret_xxx'
	})
	.then(function() {
		console.log('TXT record set');
	})
	.catch(function() {
		console.log('Failed to set TXT record');
	});
```

See acme-dns-01-test for more implementation details.

# Tests

```bash
# node ./test.js domain-zone username api-token
node ./test.js example.com me xxxxxx
```

# Authors

-   AJ ONeal

See AUTHORS for contact info.

# Legal

[acme-dns-01-namedotcom.js](https://git.coolaj86.com/coolaj86/acme-dns-01-namedotcom.js) | MPL-2.0 | [Terms of Use](https://therootcompany.com/legal/#terms) | [Privacy Policy](https://therootcompany.com/legal/#privacy)

Copyright 2019 AJ ONeal
Copyright 2019 The Root Group LLC
