#!/bin/bash
set -e

# Before assuming there's something wrong with this plugin,
# you should test that you can use your token with curl first.

echo "USERNAME: '$USERNAME'"
echo "TOKEN: '$TOKEN'"
curl -f -u "$USERNAME:$TOKEN" https://api.name.com/v4/domains
