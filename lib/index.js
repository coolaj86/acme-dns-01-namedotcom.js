"use strict";

var request;
var defaults = {
  baseUrl: "https://api.name.com/v4/",
  wait: 15 * 1000
};

module.exports.create = function(config) {
  var baseUrl = (config.baseUrl || defaults.baseUrl).replace(/\/$/, "");
  var token = config.token;
  var username = config.username;
  var wait = config.wait || defaults.wait;

  var plugin = {
    init: function(opts) {
      request = opts.request;
      return null;
    },

    // We must list all zones (domains) in the account
    zones: function(data) {
      return api({
        url: baseUrl + "/domains"
      }).then(function(resp) {
        return resp.body.domains.map(function(d) {
          //#console.log("Domain Name:", d.domainName);
          return d.domainName;
        });
      });
    },

    // We must set each record as required
    set: function(data) {
      // console.log('Add TXT', data);
      var ch = data.challenge;
      if (!ch.dnsZone) {
        throw new Error(
          "[Name.com Plugin] Unknown domain: ",
          data.domain || data.dnsHost
        );
      }
      return api({
        method: "POST",
        url: baseUrl + "/domains/" + ch.dnsZone + "/records",
        json: {
          host: ch.dnsPrefix,
          type: "TXT",
          answer: ch.dnsAuthorization,
          ttl: 300 // minimum allowed value
        }
      }).then(function(resp) {
        if (!resp.body.id) {
          throw Error("[Name.com API] [set] " + resp.body);
        }
        // The api returns success long before the record is actually set.
        // Therefore we wait a bit to make sure the server propagate the response.
        return new Promise(function(resolve) {
          setTimeout(resolve, wait, null);
        });
      });
    },

    // We must be able to confirm that the appropriate records were set
    get: function(data) {
      // console.log('List TXT', data);
      var ch = data.challenge;
      if (!ch.dnsZone) {
        throw new Error(
          "[Name.com Plugin] Unknown domain: ",
          data.domain || data.dnsHost
        );
      }
      return api({
        url: baseUrl + "/domains/" + ch.dnsZone + "/records"
      }).then(function(resp) {
        var value = resp.body.records.filter(function(r) {
          return (
            r.host === ch.dnsPrefix &&
            "TXT" === r.type &&
            ch.dnsAuthorization === r.answer
          );
        })[0];
        if (!value) {
          return null;
        }
        // adding id to make re-usable for remove
        return { id: value.id, dnsAuthorization: value.answer };
      });
    },

    // We must delete junk records once we're done
    remove: function(data) {
      // console.log('Remove TXT', data);
      var ch = data.challenge;
      return plugin.get(data).then(function(r) {
        if (!r.id) {
          throw new Error(
            "[Name.com Plugin] [del] Did not find TXT record for " + ch.dnsHost
          );
        }
        return api({
          method: "DELETE",
          url: baseUrl + "/domains/" + ch.dnsZone + "/records/" + r.id
        }).then(function(resp) {
          return null;
        });
      });
    }
  };

  // Authentication and Error handling here
  function api(opts) {
    opts.auth = {
      user: username,
      pass: token,
      sendImmediately: true
    };
    if (!opts.json) {
      opts.json = true;
    }
    return request(opts).then(function(resp) {
      if (!resp.body.message) {
        return resp;
      }

      console.error(opts.method + " " + opts.url);
      console.error(resp.headers);
      console.error(resp.body);
      throw new Error(
        "[Name.com API] " +
          (opts.method || "GET") +
          " " +
          opts.url +
          " : " +
          resp.body.message
      );
    });
  }

  return plugin;
};
